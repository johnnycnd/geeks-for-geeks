#include<iostream>
using namespace std;

int funcion(int arreglo[], int n);

int main(){
	
	int n, t;
	
	cin >> t;
	
	while(t--){
	cin >> n;
	
	int arreglo[n];
	
	for(int i = 0; i < n; i++)
		cin >> arreglo[i];
	
	cout << funcion(arreglo, n) << '\n';
	cin.ignore();
}
	return 0;
}

int funcion(int arreglo[], int n){
	int primera = 0, segunda = 0;
	
	for(int i = 0; i < n/2; i++)
		primera += arreglo[i];
	
	for(int i = n/2; i < n; i++)
		segunda += arreglo[i];
	
	if(primera > segunda){
		/*if((primera - segunda)%2 == 1)
			return (primera - segunda) - 1;
		else*/
			return primera - segunda;
	}
	
	else if(primera < segunda){
		/*if((segunda - primera)%2 == 1)
			return (segunda - primera) - 1;
		else*/
			return segunda - primera;
	
	
}
}
