#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;


int main(){
//	vector<int> arreglo;
	
	int cases, size, index;
	
	cin>>cases;
	vector<int>::iterator it;
	
	while(cases--){
		int element;
		cin>>size>>index;
		
		vector<int> arreglo;
		
		for(int i = 0; i <size; i++){
			cin>>element;
			arreglo.push_back(element);
		}
		
		std::rotate(arreglo.begin(),arreglo.begin()+index,arreglo.end());
		
		for (it=arreglo.begin(); it!=arreglo.end(); ++it)
    		cout << *it << ' ' ;
    		
    	cout << '\n';
	}
	
	
	
	return 0;
}
