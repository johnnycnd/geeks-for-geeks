#include<bits/stdc++.h>
using namespace std;

int main() {
	ios_base::sync_with_stdio(false); cin.tie(NULL);
    
	int t;
	cin>>t;
	while(t--){
	    cin.ignore();
	    int x, y, sumx=0, sumy=0, n;
	    cin>>x>>y; 
	    cin>>n; 
	    n=n*2;
	    int a[n];
	    for(int i=0;i<n;i++)
	    {
	        cin>>a[i];
	    }
	    for(int i=0;i<n-1;i+=2)
	    {
	        sumx+=a[i];
	        sumy+=a[i+1];
	    }
	    cout<<x-sumx<<" "<<y-sumy<<endl;
	}
	return 0;
}
