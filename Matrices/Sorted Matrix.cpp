#include<bits/stdc++.h>
using namespace std;
int main(){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    
	int t, n;
	
	cin >> t;
	
	while(t--){
	    cin.ignore();
	    cin >> n;
	    
	    int mat[n][n];
	    
	    for(int i =0; i <n; i++){
	        for(int j =0; j < n; j++){
	            cin >> mat[i][j];
	        }
	    }
	    
  		sort(&mat[0][0], &mat[0][0]+n*n);
	    
	    for(int i =0; i <n; i++){
	        for(int j =0; j < n; j++){
	            cout << mat[i][j] << " ";
	        }
	    }
	    cout << '\n';
	    
	}
	
	return 0;
}
