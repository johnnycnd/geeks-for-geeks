#include<iostream>
#include<stack>
#include<queue>
using namespace std;


queue<long long int> rev(queue<long long int> q);

int main(){
    long long int test;
    cin>>test; 
    while(test--)
    {
    queue<long long int> q; 
    long long int n, var; 
    cin>>n; 
    while(n--)
    {
        cin>>var; 
        q.push(var);
    }
    queue<long long int> a=rev(q); 
    while(!a.empty())
    {
        cout<<a.front()<<" ";
        a.pop();
    }
    cout<<endl; 
    }
}

queue<long long int> rev(queue<long long int> q)
{
    stack<long long int> pila; 
    while (!q.empty()) { 
        pila.push(q.front()); 
        q.pop(); 
    } 
    while (!pila.empty()) { 
        q.push(pila.top()); 
        pila.pop(); 
    }
    
    return q;
}
