#include<iostream>
#include<stack>
#include<queue>
using namespace std;
#define ll long long

queue<ll> modifyQueue(queue<ll> q, int k);
int main(){
    ll t;
    cin>>t;
    while(t-->0){
        ll n,k;
        cin>>n>>k;
        queue<ll> q;
        while(n-->0){
            ll a;
            cin>>a;
            q.push(a);
        }
        queue<ll> ans=modifyQueue(q,k);
        while(!ans.empty()){
            int a=ans.front();
            ans.pop();
            cout<<a<<" ";
        }
        cout<<endl;
    }
}


queue<ll> modifyQueue(queue<ll> q, int k)
{
  int r= q.size(); 
	stack<ll>s;
	
	for(int i=0;i<k;i++){
		s.push(q.front());
		q.pop();
	}
	
	for(int  i= 0 ;i < k; i++){
		q.push(s.top()); 
		s.pop();
	}

	for(int i=0; i < r-k; i++){
		int  x = q.front();
		q.pop();
		q.push(x); 
}

return q;
}



